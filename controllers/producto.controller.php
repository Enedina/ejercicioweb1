<?php
require_once 'models/producto.php';

class ProductoController{
	
	private $model;
	public function __CONSTRUCT(){
		$this->model = new Producto();
	}
	
	public function Index(){
		require_once 'views/producto.php';
	}
	
	public function Crud(){
		$alm = new Producto();
		if (isset($_REQUEST['idproducto'])){
			$alm = $this->model->getting($_REQUEST['idproducto']);
		}
		require_once 'views/producto-editar.php';
	
		
	}
	
	public function Guardar(){
		$alm = new Producto();
		$alm->idproducto = $_REQUEST['idproducto'];
		$alm->nombre_producto = $_REQUEST['nombre_producto'];
		$alm->precio_producto = $_REQUEST['precio_producto'];
		
		if($alm->precio_producto>=0){
			$alm->valor_iva = $alm->precio_producto * 0.16;
		}
		
		if($alm->valor_iva>=0){
			$alm->precio_iva = $alm->precio_producto + $alm->valor_iva;
		}
		
		$alm->idproducto > 0 
		? $this->model->Actualizar($alm):
		$this->model->Registrar($alm);
		
		header('Location: index.php');
		
	}
	
	public function Eliminar(){
        $this->model->Eliminar($_REQUEST['idproducto']);
        header('Location: index.php');
    }
}