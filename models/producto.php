<?php
class Producto{
	
	private $pdo;
	public $idproducto;
	public $nombre_producto;
	public $precio_producto;
	public $valor_iva;
	public $precio_iva;
	
	public function __CONSTRUCT(){
		try{
			$this->pdo = Conexion::StartUp();
		}
		catch(Exception $e){
			die($e->getMessage());
		}	
	}
	
	public function Listar(){
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT * FROM productos");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		
		catch(Exception $e){
			die($e->getMessage());
		}
		
	}
	
	public function getting($idproducto){
		try{
			$stm = $this->pdo
			->prepare("SELECT * FROM productos WHERE idproducto = ?");
			$stm->execute(array($idproducto));
			return $stm->fetch(PDO::FETCH_OBJ);
		}
		 
		catch (Exception $e){
			die($e->getMessage());
		}
	}
	
	public function Eliminar($idproducto){
		try {
			$stm = $this->pdo->prepare("DELETE FROM productos WHERE idproducto = ?");			          
			$stm->execute(array($idproducto));
		} 
		catch (Exception $e){
			die($e->getMessage());
		}
	}
	
	public function Actualizar($data){
		try{
			$sql = "UPDATE productos SET 
			nombre_producto	=?, 
			precio_producto	=?,
			valor_iva	=?,
		    precio_iva	=?
			WHERE idproducto = ?";
			$this->pdo->prepare($sql)
			->execute(
			array(
			$data->nombre_producto,
			$data->precio_producto,
			$data->valor_iva,
			$data->precio_iva,
			$data->idproducto
			)
			);	
		}
		
		catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
	
	public function Registrar($data){
		try{
			$sql = "INSERT INTO productos (nombre_producto,precio_producto,valor_iva,precio_iva) 
			VALUES (?, ?, ?,?)";
			$this->pdo->prepare($sql)
			->execute(
			array(
			$data->nombre_producto,
			$data->precio_producto,
			$data->valor_iva,
			$data->precio_iva
			)
			);
		}
		catch (Exception $e){
			die($e->getMessage());
		}
	}	
	
}

?>
