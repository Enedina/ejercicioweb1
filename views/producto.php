<h1>Lista de Productos</h1>
<div>
	<a href="?c=Producto&a=Crud">Agregar un Nuevo Producto</a>
</div>
<br />

<table>
	<thead>
		<tr>
			<th>Nombre producto</th>
			<th>Precio del producto</th>
			<th>Iva del producto</th>
			<th>Precio con iva</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($this->model->Listar() as $r): ?>
		<tr>
			<td><?php echo $r->nombre_producto; ?></td>
			<td><?php echo $r->precio_producto; ?></td>
            <td><?php echo $r->valor_iva; ?></td>
            <td><?php echo $r->precio_iva; ?></td>
            <td>
            	<i><a href="?c=Producto&a=Crud&idproducto=<?php echo $r->idproducto; ?>"> Editar</a></i>
            	
            </td>
            
            <td>
            	<i><a href="?c=Producto&a=Eliminar&idproducto=<?php echo $r->idproducto; ?>"> Eliminar</a></i>
            </td>
            	
		</tr>
		<?php endforeach; ?>
		
	</tbody>
		
</table>




		